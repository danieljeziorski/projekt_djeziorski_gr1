﻿using System;
using System.Windows.Forms;
using System.Net.Http;
using System.Collections.Generic;

using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Linq;
using System.Text;

namespace projekt_djeziorski
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();       
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = DateTime.Now.ToString("yyyy/MM/dd");
            
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.Columns.Add("Code", 50);            
            listView1.Columns.Add("Currency name", 125);            
            listView1.Columns.Add("Bid",85);
            listView1.Columns.Add("Ask", 85);
            Default_Action();
        }

        public void GetCurrency(string currencycode)
        {
            string[] ratedata = new string[4];
            ListViewItem currencies;
            
            WebRequest request = HttpWebRequest.Create("http://api.nbp.pl/api/exchangerates/rates/c/" + currencycode + "/?format=json");
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string jsonresp = reader.ReadToEnd();

            RootObject urlcurrency = JsonConvert.DeserializeObject<RootObject>(jsonresp);
            var currdata = urlcurrency.rates.LastOrDefault();
            ratedata[0] = urlcurrency.code;
            ratedata[1] = urlcurrency.currency;
            ratedata[2] = currdata.bid.ToString();
            ratedata[3] = currdata.ask.ToString();

            currencies = new ListViewItem(ratedata);
            
            listView1.Items.Add(currencies);
            textBox2.Text = currdata.effectiveDate.ToString();           
        }
        
        private void Default_Action()
        {
            GetCurrency("USD");
            GetCurrency("EUR");
            GetCurrency("CHF");
            GetCurrency("GBP");
            GetCurrency("AUD");
            GetCurrency("CAD");            
        }
        
        
        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://api.nbp.pl/");
        }

        private void GetEuropebtn_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            GetCurrency("EUR");
            GetCurrency("HUF");
            GetCurrency("CHF");
            GetCurrency("CZK");
            GetCurrency("GBP");
        }

        private void GetOtherbn_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            GetCurrency("USD");
            GetCurrency("AUD");
            GetCurrency("JPY");
            GetCurrency("CAD");
            GetCurrency("XDR");            
        }

        private void Refreshbtn_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            Default_Action();
        }

        private void Exitbtn_Click(object sender, EventArgs e) => Application.Exit();

              

        private void Button1_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(textBox3.Text))
                {
                    Msg("Wrong name. Please try again.","Wrong name!");
                }
                else
                {
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +"//"+ textBox3.Text + ".txt";
                    if (!File.Exists(path))
                    {
                        StreamWriter sw = File.CreateText(path);
                        sw.WriteLine("File creted on: "+textBox1.Text);
                        sw.WriteLine("Currencies updated on: "+textBox2.Text);
                        foreach (ListViewItem item in listView1.Items)
                        {
                            sw.WriteLine("{0} | {1}", item.SubItems[0].Text, item.SubItems[2].Text);
                        }
                        sw.WriteLine();
                        sw.Close();
                        Msg("Successfully created a file.","Success!");
                    }
                    else
                    {
                        Msg("There is already a file with the same name. Please try again.","Wrong name!");
                    }
                }

            }
        }
        private static void Msg(string message, string caption)
        {  
            MessageBox.Show(message, caption, MessageBoxButtons.OK);
        }
        
    }

}
