﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace projekt_djeziorski.Ref
{
    public class Ref
    {
        public static string GetProp()
        {
            try
            {
                PropertyInfo prep = typeof(Rate).GetProperty("effectiveDate", BindingFlags.NonPublic | BindingFlags.Instance);
                return prep.ToString();
            }
            catch
            {
                string prep = "nastąpił błąd";
                return prep;
            }
        }
    }
}
