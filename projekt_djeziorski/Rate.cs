﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt_djeziorski
{
    public class Rate
    {
        [JsonProperty("no")]
        public string no { get; set; }

        [JsonProperty("effectiveDate")]
        public string effectiveDate { get; set; }

        [JsonProperty("bid")]
        public double bid { get; set; }

        [JsonProperty("ask")]
        public double ask { get; set; }
    }
}
